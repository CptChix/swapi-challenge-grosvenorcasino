import '../styles/index.scss';
import { Starships } from './components/Starships';
import { SwapiConnector } from "./providers/swapiConnector";


const APP_ROOT_ID = 'app';

const rootDOMEl = document.getElementById(APP_ROOT_ID);

const connector = new SwapiConnector();

let starships = {};

connector.getAllStarships()
    .then((res) => {
        starships = res;

        connector.getAllFilms()
            .then (
                (films) => {
                    starships = new Starships(starships, films.results);
                    rootDOMEl.innerHTML = starships.render();
                });
    });