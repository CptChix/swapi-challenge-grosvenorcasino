export class SwapiConnector {

    constructor() {
        this.ITEMS_PER_API_CALL = 10;
    }

    getAllFilms() {
        return this.getAllItems('films');
    }

    getAllStarships() {
        return this.getAllItems('starships');
    }

    getAllItems(apiUrl) {
        return fetch('https://swapi.co/api/' + apiUrl + '/')
            .then(response => response.json())
            .then(
                (response) => {
                    const totalApiCallsNeeded = Math.ceil(response.count / this.ITEMS_PER_API_CALL);
                    let items = response;
                    if (totalApiCallsNeeded > 1) {
                        const apiPromises = [];

                        for (let i = 2; i <= totalApiCallsNeeded; i++) {
                            apiPromises.push(
                                fetch('https://swapi.co/api/' + apiUrl + '/?page=' + i).then(response => {
                                    return response.json();
                                })
                            );
                        }

                        return Promise.all(apiPromises)
                            .then(responses => {
                                responses.map(res => items.results.push(...res.results));
                                return items;
                            });
                    } else {
                        return items;
                    }
                });
    }
}
