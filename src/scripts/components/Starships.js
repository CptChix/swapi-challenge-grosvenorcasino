import { Starship } from "./Starship";

export class Starships {
    constructor(json, filmData = null) {
        this.results = json.results.map(starshipJson => new Starship(starshipJson, filmData));

        this.filtersShipsByCrewValueMin(10);

        this.sortShipsByCrewAsc();

        this.count = this.results.length;

        this.shipInMostFilms();
    }

    render() {
        let template =  `
          <div class="heading">
            <div class="heading__container">
                <div role="Heading" class="heading__text">Ships</div>
            </div>
          </div>
          <div class="starships">
        `;
        this.results.map(starship => template += starship.render());
        template += '</div>';

        return template;
    }


    // Filter ships with a crew less than `value`
    filtersShipsByCrewValueMin(value) {
        this.results = this.results.filter(starship => starship.crew > value);
    }

    // Sort results by crew value - ascending
    sortShipsByCrewAsc() {
        this.results.sort((a, b) => (parseInt(a.crew) > parseInt(b.crew)) ? 1 : -1);
    }

    shipInMostFilms() {
        let maxFilmCount = 0;
        let maxObjRef = {};
        this.results.forEach(function(obj){
            if (obj.film_count > maxFilmCount) {
                maxFilmCount = obj.film_count;
                maxObjRef = obj;
            }
        });
        maxObjRef.appearedInMostFilms = true;
    }
}