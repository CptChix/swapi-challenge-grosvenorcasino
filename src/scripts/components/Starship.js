export class Starship {

    constructor(json, allFilms = null) {
        this.name = json.name;
        this.model = json.model;
        this.crew = json.crew;
        this.passengers = json.passengers;
        this.film_count = json.films.length;

        if (allFilms) {
            this.films = [];
            this.appendFilmData(json.films, allFilms);
        } else {
            this.films = json.films;
        }
    }

    render() {
        return `
                <div class="starship${this.appearedInMostFilms ? ' starship--appearedInMostFilms' : ''}" aria-roledescription="Starship">
                  <div class="starship__name" aria-roledescription="Name">
                    ${this.name}
                  </div>
                  <div class="starship__model" aria-roledescription="Model">
                    ${this.model}
                  </div>
                  
                  <div class="starship__divider"></div>
                  
                  <div class="starship__stats" aria-roledescription="Statistics">
                    <div class="starship__stat">
                      <div class="starship__stat__value">${this.crew}</div>
                      <div class="starship__stat__name">Crew</div>
                    </div>
                    <div class="starship__stat">
                      <div class="starship__stat__value">${this.passengers}</div>
                      <div class="starship__stat__name">Passengers</div>
                    </div>
                    <div class="starship__stat">
                      <div class="starship__stat__value">${this.film_count}</div>
                      <div class="starship__stat__name">Films</div>
                    </div>
                  </div>
                </div>
        `;
    }

    appendFilmData(filmUrls, allFilms) {
        this.films = allFilms
            .filter(film => filmUrls.includes(film.url))
            .map(film => {
                return {
                    title: film.title,
                    director: film.director,
                    release_date: film.release_date
                };
            });
    }
}